/******************************************************************************

 @file       crp_gatt_profile.h

 @brief This file contains the CRP GATT profile definitions and prototypes
        prototypes.

 Group: CMCU, SCS
 Target Device: CC2640R2

 ******************************************************************************
 
 Copyright (c) 2010-2017, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************************************************/

#ifndef CRPGATTPROFILE_H
#define CRPGATTPROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Profile Parameters
#define CONTROL_MESSAGE                 0
#define SW_VERSION_MESSAGE              1
#define HW_VERSION_MESSAGE              2
#define SERIAL_NUMBER_MESSAGE           3
#define ENTER_MAINTENANCE_MESSAGE       4
#define MEASUREMENT_MESSAGE             5
#define ENABLE_MEASUREMENTS_MESSAGE     6
#define SYSTEM_ID_MESSAGE               7
#define CAR_CORNER_MESSAGE              8
#define BATTERY_STATUS_MESSAGE          9
#define RAW_MEASUREMENT_MESSAGE         10
#define ZERO_OFFSET_MESSAGE             11
#define AUTO_MEAS_PERIOD_MESSAGE        12
#define CRYPTO_KEY_MESSAGE              13
#define CALIBRATION_0_MESSAGE           14
#define CALIBRATION_1_MESSAGE           15
#define CALIBRATION_2_MESSAGE           16
#define CALIBRATION_3_MESSAGE           17
#define CALIBRATION_4_MESSAGE           18
#define CALIBRATION_5_MESSAGE           19
#define CALIBRATION_6_MESSAGE           20
#define CALIBRATION_7_MESSAGE           21
#define CALIBRATION_8_MESSAGE           22
#define CALIBRATION_9_MESSAGE           23
#define CALIBRATION_10_MESSAGE          24
#define CALIBRATION_11_MESSAGE          25
#define CALIBRATION_12_MESSAGE          26
#define CALIBRATION_13_MESSAGE          27
#define CALIBRATION_14_MESSAGE          28
#define CALIBRATION_15_MESSAGE          29
#define CALIBRATION_16_MESSAGE          30
#define CALIBRATION_17_MESSAGE          31
#define CALIBRATION_18_MESSAGE          32
#define CALIBRATION_19_MESSAGE          33

// CRP Profile Service UUID
#define CONTROL_MESSAGE_UUID            0x1234
#define SW_VERSION_MESSAGE_UUID         0x1235
#define HW_VERSION_MESSAGE_UUID         0x1236
#define SERIAL_NUMBER_MESSAGE_UUID      0x1237
#define ENABLE_MAINTENANCE_MESSAGE_UUID 0x1238
#define MEASUREMENT_MESSAGE_UUID        0x1239
//#define MEASUREMENT_INTERVAL_UUID       0x123A  //unused
#define ENABLE_MEASUREMENTS_UUID        0x123B
#define CRP_SYSTEM_ID_UUID              0x123C
#define CAR_CORNER_UUID                 0x123D
#define BATTERY_STATUS_UUID             0x123E
#define AUTO_MEAS_PER_UUID              0x123F
#define CRYPTO_KEY_UUID                 0x1240
#define RAW_MEASUREMENT_UUID            0x1241
#define ZERO_OFFSET_UUID                0x1242
// next calibration:
#define CALIBRATION_0_UUID              0x1243
#define CALIBRATION_1_UUID              0x1244
#define CALIBRATION_2_UUID              0x1245
#define CALIBRATION_3_UUID              0x1246
#define CALIBRATION_4_UUID              0x1247
#define CALIBRATION_5_UUID              0x1248
#define CALIBRATION_6_UUID              0x1249
#define CALIBRATION_7_UUID              0x124A
#define CALIBRATION_8_UUID              0x124B
#define CALIBRATION_9_UUID              0x124C
#define CALIBRATION_10_UUID             0x124D
#define CALIBRATION_11_UUID             0x124E
#define CALIBRATION_12_UUID             0x124F
#define CALIBRATION_13_UUID             0x1250
#define CALIBRATION_14_UUID             0x1251
#define CALIBRATION_15_UUID             0x1252
#define CALIBRATION_16_UUID             0x1253
#define CALIBRATION_17_UUID             0x1254
#define CALIBRATION_18_UUID             0x1255
#define CALIBRATION_19_UUID             0x1256


// Profile Service UUID
#define CRPSENSOR_PROFILE_UUID          0x0102
#define CRPSENSOR_DATA_UUID             0xFFF0
#define CRPSENSOR_DEBUG_SERVICE_UUID    0xFFF1
#define CRPSENSOR_CALIBRATION_UUID      0xFFF2

// Profile Services bit fields
#define CRPSENSOR_SERVICE               0x00000001

// Length of Message in bytes
#define ENABLE_MEASUREMENTS_MSG_LENGTH  1
#define CONTROL_MSG_LENGTH              5
#define SW_VERSION_MSG_LENGTH           5
#define HW_VERSION_MSG_LENGTH           5
#define SERIAL_NUMBER_MSG_LENGTH        8
#define MEASUREMENT_MSG_LENGTH          8 // 4  // 16.16 format
#define SYSTEM_ID_MSG_LENGTH            4
#define ENABLE_MAINTENANCE_MSG_LENGTH   1
#define CAR_CORNER_MSG_LENGTH           1
#define BATTERY_STATUS_MSG_LENGTH       1
#define RAW_MEASUREMENT_MSG_LENGTH      6
#define AUTO_MEAS_PERIOD_MSG_LENGTH         1
#define CRYPTO_KEY_MSG_LENGTH               16
#define FLASH_INITIALIZED_LENGTH        1
#define ZERO_OFFSET_MSG_LENGTH          4

#define CALIBRATION_MSG_LENGTH          8

// Flash storage location of each item
#define SW_VERSION_FLASH_LOCATION           0x80
#define HW_VERSION_FLASH_LOCATION           0x81
#define SERIAL_NUMBER_FLASH_LOCATION        0x82
#define CAR_CORNER_FLASH_LOCATION           0x83
#define SYSTEM_ID_FLASH_LOCATION            0x84
#define CRYPTO_KEY_FLASH_LOCATION           0x85
//#define CALIBRATION_OFFSET_FLASH_LOCATION 0x86
#define FLASH_INITIALIZED_LOCATION          0x87
//#define ZERO_OFFSET_FLASH_LOCATION          0x88
#define CALIBRATION_FLASH_LOCATION          0x89


/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*CRPSensorProfileChange_t)( uint8 paramID );

typedef struct
{
    CRPSensorProfileChange_t        pfnProfileChange;  // Called when characteristic value changes
} CRPSensorProfileCBs_t;



// Hack functions to for the fresh/stale/etc bit
extern uint8_t CRPSensorProfile_MeasurementRead();
extern void    CRPSensorProfile_ClearMeasurement();
extern void    CRPSensorProfile_SetFreshBit();
extern void    CRPSensorProfile_SetStaleBit();
extern void    CRPSensorProfile_SetBusyBit();
extern void    CRPSensorProfile_SetIdleBit();
extern void    CRPSensorProfile_SetTimeoutBit();
extern void    CRPSensorProfile_ClearTimeoutBit();


/*
 * CRPSensorProfile_AddService- Initializes the Simple GATT Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */

extern bStatus_t CRPSensorProfile_AddService( uint32 services );

/*
 * CRPSensorProfile_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t CRPSensorProfile_RegisterAppCBs( CRPSensorProfileCBs_t *appCallbacks );

/*
 * CRPSensorProfile_SetParameter - Set a Simple GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 */
extern bStatus_t CRPSensorProfile_SetParameter( uint8 param, uint8 len, void *value );

/*
 * CRPSensorProfile_GetParameter - Get a Simple GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 */
extern bStatus_t CRPSensorProfile_GetParameter( uint8 param, void *value );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* CRPGATTPROFILE_H */
