/******************************************************************************

 @file       crp_gatt_profile.c

 @brief This file contains the CRP GATT profile sample GATT service profile
        for use with the BLE sample application.

 Group: CMCU, SCS
 Target Device: CC2640R2

 ******************************************************************************
 
 Copyright (c) 2010-2017, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include <icall.h>
#include "util.h"
/* This Header file contains all BLE API and icall structure definition */
#include "icall_ble_api.h"

#include "crp_gatt_profile.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        114

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// CRP GATT Profile Service UUID
CONST uint8 CRPProfileServUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CRPSENSOR_PROFILE_UUID), HI_UINT16(CRPSENSOR_PROFILE_UUID)
};

// Debug Message UUID
CONST uint8 CRPProfileDebugMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CONTROL_MESSAGE_UUID), HI_UINT16(CONTROL_MESSAGE_UUID)
};

// Software Version Message UUID
CONST uint8 CRPProfileSWVersionMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(SW_VERSION_MESSAGE_UUID), HI_UINT16(SW_VERSION_MESSAGE_UUID)
};

// Hardware Version Message UUID
CONST uint8 CRPProfileHWVersionMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(HW_VERSION_MESSAGE_UUID), HI_UINT16(HW_VERSION_MESSAGE_UUID)
};

// Serial Number Message UUID
CONST uint8 CRPProfileSerialNumberMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(SERIAL_NUMBER_MESSAGE_UUID), HI_UINT16(SERIAL_NUMBER_MESSAGE_UUID)
};

// Enable Maintenance Mode Message UUID
CONST uint8 CRPProfileEnableMaintenanceMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(ENABLE_MAINTENANCE_MESSAGE_UUID), HI_UINT16(ENABLE_MAINTENANCE_MESSAGE_UUID)
};

// Read Measurement Message UUID
CONST uint8 CRPProfileMeasurementMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(MEASUREMENT_MESSAGE_UUID), HI_UINT16(MEASUREMENT_MESSAGE_UUID)
};

CONST uint8 CRPProfileEnableMeasurementsMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(ENABLE_MEASUREMENTS_UUID), HI_UINT16(ENABLE_MEASUREMENTS_UUID)
};

CONST uint8 CRPProfileSystemIDMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CRP_SYSTEM_ID_UUID), HI_UINT16(CRP_SYSTEM_ID_UUID)
};

CONST uint8 CRPProfileCarCornerMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CAR_CORNER_UUID), HI_UINT16(CAR_CORNER_UUID)
};

CONST uint8 CRPProfileBatteryStatusMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(BATTERY_STATUS_UUID), HI_UINT16(BATTERY_STATUS_UUID)
};

CONST uint8 CRPProfileRawMeasurementMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(RAW_MEASUREMENT_UUID), HI_UINT16(RAW_MEASUREMENT_UUID)
};

CONST uint8 CRPProfileAutoMeasurementPeriodMessageUUID[ATT_BT_UUID_SIZE] =
{
	LO_UINT16(AUTO_MEAS_PER_UUID), HI_UINT16(AUTO_MEAS_PER_UUID)
};

CONST uint8 CRPProfileCryptoKeyMessageUUID[ATT_BT_UUID_SIZE] =
{
	LO_UINT16(CRYPTO_KEY_UUID), HI_UINT16(CRYPTO_KEY_UUID)
};

CONST uint8 CRPProfileZeroOffsetMessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(ZERO_OFFSET_UUID), HI_UINT16(ZERO_OFFSET_UUID)
};

CONST uint8 CRPProfileCalibration0MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_0_UUID), HI_UINT16(CALIBRATION_0_UUID)
};

CONST uint8 CRPProfileCalibration1MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_1_UUID), HI_UINT16(CALIBRATION_1_UUID)
};

CONST uint8 CRPProfileCalibration2MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_2_UUID), HI_UINT16(CALIBRATION_2_UUID)
};

CONST uint8 CRPProfileCalibration3MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_3_UUID), HI_UINT16(CALIBRATION_3_UUID)
};

CONST uint8 CRPProfileCalibration4MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_4_UUID), HI_UINT16(CALIBRATION_4_UUID)
};

CONST uint8 CRPProfileCalibration5MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_5_UUID), HI_UINT16(CALIBRATION_5_UUID)
};

CONST uint8 CRPProfileCalibration6MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_6_UUID), HI_UINT16(CALIBRATION_6_UUID)
};

CONST uint8 CRPProfileCalibration7MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_7_UUID), HI_UINT16(CALIBRATION_7_UUID)
};

CONST uint8 CRPProfileCalibration8MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_8_UUID), HI_UINT16(CALIBRATION_8_UUID)
};

CONST uint8 CRPProfileCalibration9MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_9_UUID), HI_UINT16(CALIBRATION_9_UUID)
};

CONST uint8 CRPProfileCalibration10MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_10_UUID), HI_UINT16(CALIBRATION_10_UUID)
};

CONST uint8 CRPProfileCalibration11MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_11_UUID), HI_UINT16(CALIBRATION_11_UUID)
};

CONST uint8 CRPProfileCalibration12MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_12_UUID), HI_UINT16(CALIBRATION_12_UUID)
};

CONST uint8 CRPProfileCalibration13MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_13_UUID), HI_UINT16(CALIBRATION_13_UUID)
};

CONST uint8 CRPProfileCalibration14MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_14_UUID), HI_UINT16(CALIBRATION_14_UUID)
};

CONST uint8 CRPProfileCalibration15MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_15_UUID), HI_UINT16(CALIBRATION_15_UUID)
};

CONST uint8 CRPProfileCalibration16MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_16_UUID), HI_UINT16(CALIBRATION_16_UUID)
};

CONST uint8 CRPProfileCalibration17MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_17_UUID), HI_UINT16(CALIBRATION_17_UUID)
};

CONST uint8 CRPProfileCalibration18MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_18_UUID), HI_UINT16(CALIBRATION_18_UUID)
};

CONST uint8 CRPProfileCalibration19MessageUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(CALIBRATION_19_UUID), HI_UINT16(CALIBRATION_19_UUID)
};

/*********************************************************************
 * LOCAL VARIABLES
 */

static CRPSensorProfileCBs_t *Profile_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// CRP Profile Service attribute
static CONST gattAttrType_t CRPSensorProfileService = { ATT_BT_UUID_SIZE, CRPProfileServUUID };


// Control Message Properties
    static uint8 ControlMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
    static uint8 ControlMessageValue[CONTROL_MSG_LENGTH] = {0,0,0,0,0};
    static uint8 ControlMessageUserDescription[7] = "Contrl\0";

// Software Version Message Properties
    static uint8 SWVersionMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
    static uint8 SWVersionMessageValue[SW_VERSION_MSG_LENGTH] = "2.2.0";
    static uint8 SWVersionMessageUserDescription[8] = "SW Vers\0";

// Hardware Version Message Properties
    static uint8 HWVersionMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
    static uint8 HWVersionMessageValue[HW_VERSION_MSG_LENGTH] = "5.0.0";
    static uint8 HWVersionMessageUserDescription[8] = "HW Vers\0";

// Serial Number Message Properties
    static uint8 SerialNumberMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
    static uint8 SerialNumberMessageValue[SERIAL_NUMBER_MSG_LENGTH] = {0,0,0,0,0,0,0,0};
    static uint8 SerialNumberMessageUserDescription[9] = "Serial #\0";

// Enable Maintenance Message Properties
    static uint8 EnableMaintenanceMessageProps = GATT_PROP_WRITE;
    static uint8 EnableMaintenanceMessageValue[ENABLE_MAINTENANCE_MSG_LENGTH] = {0,};
    static uint8 EnableMaintenanceMessageUserDescription[1] = "\0";

// Measurement Message Properties
    static gattCharCfg_t *MeasurementMessageConfig;
    static uint8 MeasurementMessageProps = GATT_PROP_READ | GATT_PROP_NOTIFY;
    static uint8 MeasurementMessageValue[MEASUREMENT_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 MeasurementMessageUserDescription[12] = "Measurement\0";

// Enable Measurements Message Properties
    static uint8 EnableMeasurementsMessageProps = GATT_PROP_WRITE;
    static uint8 EnableMeasurementsMessageValue[ENABLE_MEASUREMENTS_MSG_LENGTH] = {0,};
    static uint8 EnableMeasurementMessageUserDescription[12] = "Enable Meas\0";

// System ID Message Properties
    static uint8 SystemIDMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
    static uint8 SystemIDMessageValue[SYSTEM_ID_MSG_LENGTH] = {1, 2, 3, 4};
    static uint8 SystemIDMessageUserDescription[7] = "Sys ID\0";

// Car Corner Message Properties
    static uint8 CarCornerMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
    static uint8 CarCornerMessageValue[CAR_CORNER_MSG_LENGTH] = {0,};
    static uint8 CarCornerMessageUserDescription[11] = "Car Corner\0";

// Battery Status Message Properties
    static uint8 BatteryStatusMessageProps = GATT_PROP_READ;
    static uint8 BatteryStatusMessageValue[BATTERY_STATUS_MSG_LENGTH] = {80,};
    static uint8 BatteryStatusMessageUserDescription[12] = "Batt Status\0";

// Raw Measurements Message Properties
    static uint8 RawMeasurementsMessageProps = GATT_PROP_READ;
    static uint8 RawMeasurementsMessageValue[RAW_MEASUREMENT_MSG_LENGTH] = {0, 0, 0, 0, 0, 0};
    static uint8 RawMeasurementsMessageUserDescription[16] = "Raw Measurement\0";

// Auto-Measurement Period Message Properties
	static uint8 AutoMeasurementPeriodMessageProps = GATT_PROP_READ | GATT_PROP_WRITE;
	static uint8 AutoMeasurementPeriodMessageValue[AUTO_MEAS_PERIOD_MSG_LENGTH] = {0};
	static uint8 AutoMeasurementPeriodMessageUserDescription[17] = "Auto Meas Period\0";
	
// Crypto Key Message Properties
	static uint8 CryptoKeyMessageProps = GATT_PROP_WRITE;
	static uint8 CryptoKeyMessageValue[CRYPTO_KEY_MSG_LENGTH] = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};
	static uint8 CryptoKeyMessageUserDescription[1] = "\0";
	
// Zero Offset Message Properties
    static gattCharCfg_t *ZeroOffsetMessageConfig;
    static uint8 ZeroOffsetMessageProps = GATT_PROP_READ | GATT_PROP_NOTIFY;
    static uint8 ZeroOffsetMessageValue[ZERO_OFFSET_MSG_LENGTH] = {0, 0, 0, 0};
    static uint8 ZeroOffsetMessageUserDescription[12] = "Zero Offset\0";

// Calibration 0 Message Properties
    static uint8 Calibration0MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration0MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration0MessageUserDescription[14] = "Calibration 0\0";

// Calibration 1 Message Properties
    static uint8 Calibration1MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration1MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration1MessageUserDescription[14] = "Calibration 1\0";

// Calibration 2 Message Properties
    static uint8 Calibration2MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration2MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration2MessageUserDescription[14] = "Calibration 2\0";

// Calibration 3 Message Properties
    static uint8 Calibration3MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration3MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration3MessageUserDescription[14] = "Calibration 3\0";

// Calibration 4 Message Properties
    static uint8 Calibration4MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration4MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration4MessageUserDescription[14] = "Calibration 4\0";

// Calibration 5 Message Properties
    static uint8 Calibration5MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration5MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration5MessageUserDescription[14] = "Calibration 5\0";

// Calibration 6 Message Properties
    static uint8 Calibration6MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration6MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration6MessageUserDescription[14] = "Calibration 6\0";

// Calibration 7 Message Properties
    static uint8 Calibration7MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration7MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration7MessageUserDescription[14] = "Calibration 7\0";

// Calibration 8 Message Properties
    static uint8 Calibration8MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration8MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration8MessageUserDescription[14] = "Calibration 8\0";

// Calibration 9 Message Properties
    static uint8 Calibration9MessageProps = GATT_PROP_READ | GATT_PROP_WRITE ;
    static uint8 Calibration9MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration9MessageUserDescription[14] = "Calibration 9\0";

// Calibration 10 Message Properties
    static uint8 Calibration10MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration10MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration10MessageUserDescription[15] = "Calibration 10\0";

// Calibration 11 Message Properties
    static uint8 Calibration11MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration11MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration11MessageUserDescription[15] = "Calibration 11\0";

// Calibration 12 Message Properties
    static uint8 Calibration12MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration12MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration12MessageUserDescription[15] = "Calibration 12\0";

// Calibration 13 Message Properties
    static uint8 Calibration13MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration13MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration13MessageUserDescription[15] = "Calibration 13\0";

// Calibration 14 Message Properties
    static uint8 Calibration14MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration14MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration14MessageUserDescription[15] = "Calibration 14\0";

// Calibration 15 Message Properties
    static uint8 Calibration15MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration15MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration15MessageUserDescription[15] = "Calibration 15\0";

// Calibration 16 Message Properties
    static uint8 Calibration16MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration16MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration16MessageUserDescription[15] = "Calibration 16\0";

// Calibration 17 Message Properties
    static uint8 Calibration17MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration17MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration17MessageUserDescription[15] = "Calibration 17\0";

// Calibration 18 Message Properties
    static uint8 Calibration18MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration18MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration18MessageUserDescription[15] = "Calibration 18\0";

// Calibration 19 Message Properties
    static uint8 Calibration19MessageProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;
    static uint8 Calibration19MessageValue[CALIBRATION_MSG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
    static uint8 Calibration19MessageUserDescription[15] = "Calibration 19\0";

static uint8_t bMeasurementRead = 0;


/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t CRPSensorProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] =
{
  // CRP Sensor Profile Service - 1
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8 *)&CRPSensorProfileService         /* pValue */
  },

  {    // Enable Measurements Declaration - 2
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &EnableMeasurementsMessageProps
  },

      // Enable Measurements Message Value - 3
      {
          { ATT_BT_UUID_SIZE, CRPProfileEnableMeasurementsMessageUUID },
          GATT_PERMIT_READ | GATT_PERMIT_WRITE,
          0,
          EnableMeasurementsMessageValue
      },

      // Enable Measurements Message User Description - 4
      {
          { ATT_BT_UUID_SIZE, charUserDescUUID },
          GATT_PERMIT_READ,
          0,
          EnableMeasurementMessageUserDescription
      },

        // Measurement Message Declaration - 5
        {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &MeasurementMessageProps
        },

            // Measurement Message Value
            {
                { ATT_BT_UUID_SIZE, CRPProfileMeasurementMessageUUID },
                GATT_PERMIT_READ,
                0,
                MeasurementMessageValue
            },

            // Measurement Message configuration
            {
                { ATT_BT_UUID_SIZE, clientCharCfgUUID },
                GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                0,
                (uint8 *)&MeasurementMessageConfig
            },

            // Measurement Message User Description
            {
                { ATT_BT_UUID_SIZE, charUserDescUUID },
                GATT_PERMIT_READ,
                0,
                MeasurementMessageUserDescription
            },

        // Battery Status Message Declaration - 9
        {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &BatteryStatusMessageProps
        },

            // Battery Status Message Value - 10
            {
                { ATT_BT_UUID_SIZE, CRPProfileBatteryStatusMessageUUID },
                GATT_PERMIT_READ,
                0,
                BatteryStatusMessageValue
            },

            // Battery Status Message User Description - 11
            {
                { ATT_BT_UUID_SIZE, charUserDescUUID },
                GATT_PERMIT_READ,
                0,
                BatteryStatusMessageUserDescription
            },

        // Zero Offset Message Declaration - 12
        {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &ZeroOffsetMessageProps
        },

            // Zero Offset Message Value - 13
            {
                { ATT_BT_UUID_SIZE, CRPProfileZeroOffsetMessageUUID },
                GATT_PERMIT_READ,
                0,
                ZeroOffsetMessageValue
            },

            // Zero Offset Message configuration - 14
            {
                { ATT_BT_UUID_SIZE, clientCharCfgUUID },
                GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                0,
                (uint8 *)&ZeroOffsetMessageConfig
            },

            // Zero Offset Message User Description - 15
            {
                { ATT_BT_UUID_SIZE, charUserDescUUID },
                GATT_PERMIT_READ,
                0,
                ZeroOffsetMessageUserDescription
            },

    // Enable Maintenance Declaration - 16
    {
        {ATT_BT_UUID_SIZE, characterUUID},
        GATT_PERMIT_READ,
        0,
        &EnableMaintenanceMessageProps
    },
        // Enable Maintenance Value - 17
        {
            { ATT_BT_UUID_SIZE, CRPProfileEnableMaintenanceMessageUUID },
            GATT_PERMIT_WRITE,
            0,
            EnableMaintenanceMessageValue
        },

        // Enable Maintenance User Description - 18
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            EnableMaintenanceMessageUserDescription
        },

        // SW Version Declaration - 19
        {
          {ATT_BT_UUID_SIZE, characterUUID},
          GATT_PERMIT_READ,
          0,
          &SWVersionMessageProps
        },

        // SW Version Value - 20
        {
            { ATT_BT_UUID_SIZE, CRPProfileSWVersionMessageUUID },
            GATT_PERMIT_READ,
            0,
            SWVersionMessageValue
        },

        // SW Version User Description - 21
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            SWVersionMessageUserDescription
        },


    // Control Message Declaration - 22
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &ControlMessageProps
    },

      // Control Message Value - 23
      {
        { ATT_BT_UUID_SIZE, CRPProfileDebugMessageUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        ControlMessageValue
      },

      // Control Message User Description - 24
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        ControlMessageUserDescription
      },

    // System ID Message Declaration - 25
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &SystemIDMessageProps
    },

        // System ID Message Value - 26
        {
            { ATT_BT_UUID_SIZE, CRPProfileSystemIDMessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            SystemIDMessageValue
        },

        // System ID Message User Description - 27
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            SystemIDMessageUserDescription
        },

    // Car Corner Message Declaration - 28
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &CarCornerMessageProps
    },

        // Car Corner Message Value -
        {
            { ATT_BT_UUID_SIZE, CRPProfileCarCornerMessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            CarCornerMessageValue
        },

        // Car Corner Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            CarCornerMessageUserDescription
        },

    // HW Version Declaration - 31
    {
        {ATT_BT_UUID_SIZE, characterUUID},
        GATT_PERMIT_READ,
        0,
        &HWVersionMessageProps
    },
        // HW Version Value -
        {
            { ATT_BT_UUID_SIZE, CRPProfileHWVersionMessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            HWVersionMessageValue
        },

        // HW Version User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            HWVersionMessageUserDescription
        },

    // Raw Measurement Message Declaration - 34
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &RawMeasurementsMessageProps
    },

        // Raw Measurement Message Value -
        {
            { ATT_BT_UUID_SIZE, CRPProfileRawMeasurementMessageUUID },
            GATT_PERMIT_READ,
            0,
            RawMeasurementsMessageValue
        },

        // Raw Measurement Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            RawMeasurementsMessageUserDescription
        },

        // AutoMeasurementPeriod Message Declaration - 38
        {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &AutoMeasurementPeriodMessageProps
        },

            // AutoMeasurementPeriod Message Value - 39
            {
                { ATT_BT_UUID_SIZE, CRPProfileAutoMeasurementPeriodMessageUUID },
                GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                0,
                AutoMeasurementPeriodMessageValue
            },

            // AutoMeasurementPeriod Message User Description - 40
            {
                { ATT_BT_UUID_SIZE, charUserDescUUID },
                GATT_PERMIT_READ,
                0,
                AutoMeasurementPeriodMessageUserDescription
            },

        // Crypto Key Message Declaration - 41
        {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &CryptoKeyMessageProps
        },

            // AES Key Message Value - 42
            {
                { ATT_BT_UUID_SIZE, CRPProfileCryptoKeyMessageUUID },
                GATT_PERMIT_WRITE,
                0,
                CryptoKeyMessageValue
            },

            // AES Key Message User Description - 43
            {
                { ATT_BT_UUID_SIZE, charUserDescUUID },
                GATT_PERMIT_READ,
                0,
                CryptoKeyMessageUserDescription
            },


    // Serial Number Declaration - 37
    {
        {ATT_BT_UUID_SIZE, characterUUID},
        GATT_PERMIT_READ,
        0,
        &SerialNumberMessageProps
    },
        // Serial Number Value
        {
            { ATT_BT_UUID_SIZE, CRPProfileSerialNumberMessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            SerialNumberMessageValue
        },

        // Serial Number User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            SerialNumberMessageUserDescription
        },

    // Calibration 0 Message Declaration - 340
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &Calibration0MessageProps
    },

        // Calibration 0 Message Value
        {
            { ATT_BT_UUID_SIZE, CRPProfileCalibration0MessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            Calibration0MessageValue
        },

        // Calibration 0 Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            Calibration0MessageUserDescription
        },

    // Calibration 1 Message Declaration - 43
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &Calibration1MessageProps
    },

        // Calibration 1 Message Value
        {
            { ATT_BT_UUID_SIZE, CRPProfileCalibration1MessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            Calibration1MessageValue
        },

        // Calibration 1 Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            Calibration1MessageUserDescription
        },

    // Calibration 2 Message Declaration - 46
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &Calibration2MessageProps
    },

        // Calibration 2 Message Value
        {
            { ATT_BT_UUID_SIZE, CRPProfileCalibration2MessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            Calibration2MessageValue
        },

        // Calibration 2 Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            Calibration2MessageUserDescription
        },

    // Calibration 3 Message Declaration - 49
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &Calibration3MessageProps
    },

        // Calibration 3 Message Value
        {
            { ATT_BT_UUID_SIZE, CRPProfileCalibration3MessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            Calibration3MessageValue
        },

        // Calibration 3 Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            Calibration3MessageUserDescription
        },

    // Calibration 4 Message Declaration - 52
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &Calibration4MessageProps
    },

        // Calibration 4 Message Value -
        {
            { ATT_BT_UUID_SIZE, CRPProfileCalibration4MessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            Calibration4MessageValue
        },

        // Calibration 4 Message User Description -
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            Calibration4MessageUserDescription
        },

    // Calibration 5 Message Declaration - 55
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &Calibration5MessageProps
    },

        // Calibration 5 Message Value
        {
            { ATT_BT_UUID_SIZE, CRPProfileCalibration5MessageUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            Calibration5MessageValue
        },

        // Calibration 5 Message User Description
        {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            Calibration5MessageUserDescription
        },

      // Calibration 6 Message Declaration - 58
      {
          { ATT_BT_UUID_SIZE, characterUUID },
          GATT_PERMIT_READ,
          0,
          &Calibration6MessageProps
      },

          // Calibration 6 Message Value -
          {
              { ATT_BT_UUID_SIZE, CRPProfileCalibration6MessageUUID },
              GATT_PERMIT_READ | GATT_PERMIT_WRITE,
              0,
              Calibration6MessageValue
          },

          // Calibration 6 Message User Description
          {
              { ATT_BT_UUID_SIZE, charUserDescUUID },
              GATT_PERMIT_READ,
              0,
              Calibration6MessageUserDescription
          },

      // Calibration 7 Message Declaration - 61
      {
          { ATT_BT_UUID_SIZE, characterUUID },
          GATT_PERMIT_READ,
          0,
          &Calibration7MessageProps
      },

          // Calibration 7 Message Value -
          {
              { ATT_BT_UUID_SIZE, CRPProfileCalibration7MessageUUID },
              GATT_PERMIT_READ | GATT_PERMIT_WRITE,
              0,
              Calibration7MessageValue
          },

          // Calibration 7 Message User Description
          {
              { ATT_BT_UUID_SIZE, charUserDescUUID },
              GATT_PERMIT_READ,
              0,
              Calibration7MessageUserDescription
          },

      // Calibration 8 Message Declaration - 64
      {
          { ATT_BT_UUID_SIZE, characterUUID },
          GATT_PERMIT_READ,
          0,
          &Calibration8MessageProps
      },

          // Calibration 8 Message Value
          {
              { ATT_BT_UUID_SIZE, CRPProfileCalibration8MessageUUID },
              GATT_PERMIT_READ | GATT_PERMIT_WRITE,
              0,
              Calibration8MessageValue
          },

          // Calibration 8 Message User Description
          {
              { ATT_BT_UUID_SIZE, charUserDescUUID },
              GATT_PERMIT_READ,
              0,
              Calibration8MessageUserDescription
          },

      // Calibration 9 Message Declaration - 67
      {
          { ATT_BT_UUID_SIZE, characterUUID },
          GATT_PERMIT_READ,
          0,
          &Calibration9MessageProps
      },

          // Calibration 9 Message Value
          {
              { ATT_BT_UUID_SIZE, CRPProfileCalibration9MessageUUID },
              GATT_PERMIT_READ | GATT_PERMIT_WRITE,
              0,
              Calibration9MessageValue
          },

          // Calibration 9 Message User Description - 69
          {
              { ATT_BT_UUID_SIZE, charUserDescUUID },
              GATT_PERMIT_READ,
              0,
              Calibration9MessageUserDescription
          },

          // Calibration 10 Message Declaration - 78
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration10MessageProps
            },

                // Calibration 10 Message Value - 79
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration10MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration10MessageValue
                },

                // Calibration 10 Message User Description - 80
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration10MessageUserDescription
                },

            // Calibration 11 Message Declaration - 81
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration11MessageProps
            },

                // Calibration 11 Message Value - 82
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration11MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration11MessageValue
                },

                // Calibration 11 Message User Description - 83
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration11MessageUserDescription
                },

            // Calibration 12 Message Declaration - 84
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration12MessageProps
            },

                // Calibration 12 Message Value - 85
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration12MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration12MessageValue
                },

                // Calibration 12 Message User Description - 86
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration12MessageUserDescription
                },

            // Calibration 13 Message Declaration - 87
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration13MessageProps
            },

                // Calibration 13 Message Value - 88
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration13MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration13MessageValue
                },

                // Calibration 9 Message User Description - 89
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration13MessageUserDescription
                },

            // Calibration 14 Message Declaration - 90
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration14MessageProps
            },

                // Calibration 14 Message Value - 91
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration14MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration14MessageValue
                },

                // Calibration 14 Message User Description - 92
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration14MessageUserDescription
                },

            // Calibration 15 Message Declaration - 93
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration15MessageProps
            },

                // Calibration 15 Message Value - 94
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration15MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration15MessageValue
                },

                // Calibration 15 Message User Description - 95
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration15MessageUserDescription
                },

            // Calibration 16 Message Declaration - 96
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration16MessageProps
            },

                // Calibration 16 Message Value - 97
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration16MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration16MessageValue
                },

                // Calibration 16 Message User Description - 98
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration16MessageUserDescription
                },

            // Calibration 17 Message Declaration - 99
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration17MessageProps
            },

                // Calibration 17 Message Value - 100
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration17MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration17MessageValue
                },

                // Calibration 17 Message User Description - 101
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration17MessageUserDescription
                },

            // Calibration 18 Message Declaration - 102
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration18MessageProps
            },

                // Calibration 18 Message Value - 103
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration18MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration18MessageValue
                },

                // Calibration 18 Message User Description - 104
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration18MessageUserDescription
                },

            // Calibration 19 Message Declaration - 105
            {
                { ATT_BT_UUID_SIZE, characterUUID },
                GATT_PERMIT_READ,
                0,
                &Calibration19MessageProps
            },

                // Calibration 19 Message Value - 106
                {
                    { ATT_BT_UUID_SIZE, CRPProfileCalibration19MessageUUID },
                    GATT_PERMIT_READ | GATT_PERMIT_WRITE,
                    0,
                    Calibration19MessageValue
                },

                // Calibration 19 Message User Description - 107
                {
                    { ATT_BT_UUID_SIZE, charUserDescUUID },
                    GATT_PERMIT_READ,
                    0,
                    Calibration19MessageUserDescription
                }
};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t Profile_ReadAttrCB( uint16_t connHandle,
                                     gattAttribute_t *pAttr,
                                     uint8_t *pValue, uint16_t *pLen,
                                     uint16_t offset, uint16_t maxLen,
                                     uint8_t method );
static bStatus_t Profile_WriteAttrCB( uint16_t connHandle,
                                      gattAttribute_t *pAttr,
                                      uint8_t *pValue, uint16_t len,
                                      uint16_t offset, uint8_t method );


/*********************************************************************
 * PROFILE CALLBACKS
 */

// CRP Profile Service Callbacks
// Note: When an operation on a characteristic requires authorization and
// pfnAuthorizeAttrCB is not defined for that characteristic's service, the
// Stack will report a status of ATT_ERR_UNLIKELY to the client.  When an
// operation on a characteristic requires authorization the Stack will call
// pfnAuthorizeAttrCB to check a client's authorization prior to calling
// pfnReadAttrCB or pfnWriteAttrCB, so no checks for authorization need to be
// made within these functions.
CONST gattServiceCBs_t ProfileCBs =
{
  Profile_ReadAttrCB,  // Read callback function pointer
  Profile_WriteAttrCB, // Write callback function pointer
  NULL                 // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      CRPProfile_AddService
 *
 * @brief   Initializes the Simple Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t CRPSensorProfile_AddService( uint32 services )
{
  uint8 status;

  //// ---- Zero Notify
  // Allocate Client Characteristic Configuration table
  ZeroOffsetMessageConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *
                                                            linkDBNumConns );
  if (ZeroOffsetMessageConfig == NULL )
  {
    return ( bleMemAllocError );
  }

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, ZeroOffsetMessageConfig);

  //// ---- Measurement Notify
  // Allocate Client Characteristic Configuration table
  MeasurementMessageConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *
                                                            linkDBNumConns );
  if (MeasurementMessageConfig == NULL )
  {
    return ( bleMemAllocError );
  }

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, MeasurementMessageConfig);


  if ( services & CRPSENSOR_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( CRPSensorProfileAttrTbl,
                                          GATT_NUM_ATTRS( CRPSensorProfileAttrTbl ),
                                          GATT_MAX_ENCRYPT_KEY_SIZE,
                                          &ProfileCBs );
  }
  else
  {
    status = SUCCESS;
  }

  return ( status );
}


/*********************************************************************
 * @fn      CRPSensorProfile_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t CRPSensorProfile_RegisterAppCBs( CRPSensorProfileCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
      Profile_AppCBs = appCallbacks;

    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}

/*********************************************************************
 * @fn      CRPSensorProfile_SetParameter
 *
 * @brief   Set a Simple Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to write
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t CRPSensorProfile_SetParameter( uint8 param, uint8 len, void *value )
{
    bStatus_t ret = SUCCESS;

        switch ( param )
        {
        case CONTROL_MESSAGE:
            if ( len <= CONTROL_MSG_LENGTH )
            {
                VOID memcpy( ControlMessageValue, value, len );
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case SW_VERSION_MESSAGE:
            if ( len <= SW_VERSION_MSG_LENGTH )
            {
                VOID memcpy( SWVersionMessageValue, value, len );
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case HW_VERSION_MESSAGE:
            if ( len <= HW_VERSION_MSG_LENGTH )
            {
                VOID memcpy( HWVersionMessageValue, value, len );
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case SERIAL_NUMBER_MESSAGE:
            if ( len <= SERIAL_NUMBER_MSG_LENGTH )
            {
                VOID memcpy( SerialNumberMessageValue, value, len );
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case ENTER_MAINTENANCE_MESSAGE:
            if ( len == sizeof ( uint8 ) )
            {
                VOID memcpy (EnableMaintenanceMessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case MEASUREMENT_MESSAGE:
            if(len == MEASUREMENT_MSG_LENGTH)
            {
                VOID memcpy (MeasurementMessageValue, value, len);

                GATTServApp_ProcessCharCfg(MeasurementMessageConfig, MeasurementMessageValue, FALSE,
                                           CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
                                           INVALID_TASK_ID, Profile_ReadAttrCB);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case ENABLE_MEASUREMENTS_MESSAGE:
            if(len == sizeof(uint8))
            {
                VOID memcpy (EnableMeasurementsMessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case SYSTEM_ID_MESSAGE:
            if(len == SYSTEM_ID_MSG_LENGTH)
            {
                VOID memcpy (SystemIDMessageValue, value, len);
    /*
                GATTServApp_ProcessCharCfg(CRPSensorConfig, SystemIDMessageValue, FALSE,
                               CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
                               INVALID_TASK_ID, Profile_ReadAttrCB);
    */
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CAR_CORNER_MESSAGE:
            if(len == sizeof(uint8))
            {
                VOID memcpy (CarCornerMessageValue, value, len);
    /*
                GATTServApp_ProcessCharCfg(CRPSensorConfig, CarCornerMessageValue, FALSE,
                               CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
                               INVALID_TASK_ID, Profile_ReadAttrCB);
    */
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case BATTERY_STATUS_MESSAGE:
            if(len == sizeof(uint8))
            {
                VOID memcpy (BatteryStatusMessageValue, value, len);
    /*
                GATTServApp_ProcessCharCfg(CRPSensorConfig, BatteryStatusMessageValue, FALSE,
                               CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
                               INVALID_TASK_ID, Profile_ReadAttrCB);
    */
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case RAW_MEASUREMENT_MESSAGE:
            if(len == RAW_MEASUREMENT_MSG_LENGTH)
            {
                VOID memcpy (RawMeasurementsMessageValue, value, len);

    /*
                GATTServApp_ProcessCharCfg(CRPSensorConfig, RawMeasurementsMessageValue, FALSE,
                               CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
                               INVALID_TASK_ID, Profile_ReadAttrCB);
*/
		}
		else
		{
			ret = bleInvalidRange;
		}
		break;
		
	case AUTO_MEAS_PERIOD_MESSAGE:
		if(len == AUTO_MEAS_PERIOD_MSG_LENGTH)
		{
		VOID memcpy (AutoMeasurementPeriodMessageValue, value, len);

/*
			GATTServApp_ProcessCharCfg(CRPSensorConfig, CalibrationSlopeMessageValue, FALSE,
						   CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
						   INVALID_TASK_ID, Profile_ReadAttrCB);
*/
		}
		else
		{
			ret = bleInvalidRange;
		}
		break;
		
	case CRYPTO_KEY_MESSAGE:
		if(len == CRYPTO_KEY_MSG_LENGTH)
		{
		VOID memcpy (CryptoKeyMessageValue, value, len);
/*
			GATTServApp_ProcessCharCfg(CRPSensorConfig, CalibrationOffsetMessageValue, FALSE,
						   CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
						   INVALID_TASK_ID, Profile_ReadAttrCB);
*/
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case ZERO_OFFSET_MESSAGE:
            if(len == ZERO_OFFSET_MSG_LENGTH)
            {
                VOID memcpy (ZeroOffsetMessageValue, value, len);

                GATTServApp_ProcessCharCfg(ZeroOffsetMessageConfig, ZeroOffsetMessageValue, FALSE,
                                           CRPSensorProfileAttrTbl, GATT_NUM_ATTRS(CRPSensorProfileAttrTbl),
                                           INVALID_TASK_ID, Profile_ReadAttrCB);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;


        case CALIBRATION_0_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration0MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_1_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration1MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_2_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration2MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_3_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration3MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_4_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration4MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_5_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration5MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_6_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration6MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_7_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration7MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_8_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration8MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_9_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration9MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_10_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration10MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_11_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration11MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_12_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration12MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_13_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration13MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_14_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration14MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_15_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration15MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_16_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration16MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_17_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration17MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_18_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration18MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        case CALIBRATION_19_MESSAGE:
            if(len == CALIBRATION_MSG_LENGTH)
            {
                VOID memcpy (Calibration19MessageValue, value, len);
            }
            else
            {
                ret = bleInvalidRange;
            }
            break;

        default:
            ret = INVALIDPARAMETER;
            break;
        }

        return ( ret );
    }

/*********************************************************************
 * @fn      CRPSensorProfile_GetParameter
 *
 * @brief   Get a Simple Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t CRPSensorProfile_GetParameter( uint8 param, void *value )
{
    bStatus_t ret = SUCCESS;
  switch ( param )
  {
      case CONTROL_MESSAGE:
          VOID memcpy( value, ControlMessageValue, CONTROL_MSG_LENGTH );
          break;

      case SW_VERSION_MESSAGE:
          VOID memcpy( value, SWVersionMessageValue, SW_VERSION_MSG_LENGTH );
          break;

      case HW_VERSION_MESSAGE:
          VOID memcpy( value, HWVersionMessageValue, HW_VERSION_MSG_LENGTH );
          break;

      case SERIAL_NUMBER_MESSAGE:
          VOID memcpy( value, SerialNumberMessageValue, SERIAL_NUMBER_MSG_LENGTH );
          break;

      case ENTER_MAINTENANCE_MESSAGE:
          VOID memcpy( value, EnableMaintenanceMessageValue, ENABLE_MEASUREMENTS_MSG_LENGTH);
          break;

      case MEASUREMENT_MESSAGE:
          VOID memcpy( value, MeasurementMessageValue, MEASUREMENT_MSG_LENGTH );
          break;

      case ENABLE_MEASUREMENTS_MESSAGE:
          VOID memcpy( value, EnableMeasurementsMessageValue, ENABLE_MEASUREMENTS_MSG_LENGTH );
          break;

      case SYSTEM_ID_MESSAGE:
          VOID memcpy(value, SystemIDMessageValue, SYSTEM_ID_MSG_LENGTH);
          break;

      case CAR_CORNER_MESSAGE:
          VOID memcpy(value, CarCornerMessageValue, CAR_CORNER_MSG_LENGTH);
          break;

      case BATTERY_STATUS_MESSAGE:
          VOID memcpy(value, BatteryStatusMessageValue, BATTERY_STATUS_MSG_LENGTH);
		  break;
		  
	  case AUTO_MEAS_PERIOD_MESSAGE:
		  VOID memcpy( value, AutoMeasurementPeriodMessageValue, AUTO_MEAS_PERIOD_MSG_LENGTH );
		  break;
		  
	  case CRYPTO_KEY_MESSAGE:
		  VOID memcpy( value, CryptoKeyMessageValue, CRYPTO_KEY_MSG_LENGTH );
          break;

      case RAW_MEASUREMENT_MESSAGE:
          VOID memcpy( value, RawMeasurementsMessageValue, RAW_MEASUREMENT_MSG_LENGTH );
          break;

      case ZERO_OFFSET_MESSAGE:
          VOID memcpy( value, ZeroOffsetMessageValue, ZERO_OFFSET_MSG_LENGTH );
          break;

      case CALIBRATION_0_MESSAGE:
          VOID memcpy( value, Calibration0MessageValue, CALIBRATION_MSG_LENGTH );
          break;

      case CALIBRATION_1_MESSAGE:
          VOID memcpy( value, Calibration1MessageValue, CALIBRATION_MSG_LENGTH );
          break;

      case CALIBRATION_2_MESSAGE:
          VOID memcpy( value, Calibration2MessageValue, CALIBRATION_MSG_LENGTH );
          break;

      case CALIBRATION_3_MESSAGE:
          VOID memcpy( value, Calibration3MessageValue, CALIBRATION_MSG_LENGTH );
          break;

      case CALIBRATION_4_MESSAGE:
          VOID memcpy( value, Calibration4MessageValue, CALIBRATION_MSG_LENGTH );
          break;

      case CALIBRATION_5_MESSAGE:
          VOID memcpy( value, Calibration5MessageValue, CALIBRATION_MSG_LENGTH );
          break;

      case CALIBRATION_6_MESSAGE:
            VOID memcpy( value, Calibration6MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_7_MESSAGE:
            VOID memcpy( value, Calibration7MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_8_MESSAGE:
            VOID memcpy( value, Calibration8MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_9_MESSAGE:
            VOID memcpy( value, Calibration9MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_10_MESSAGE:
            VOID memcpy( value, Calibration10MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_11_MESSAGE:
            VOID memcpy( value, Calibration11MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_12_MESSAGE:
            VOID memcpy( value, Calibration12MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_13_MESSAGE:
            VOID memcpy( value, Calibration13MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_14_MESSAGE:
            VOID memcpy( value, Calibration14MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_15_MESSAGE:
            VOID memcpy( value, Calibration15MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_16_MESSAGE:
            VOID memcpy( value, Calibration16MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_17_MESSAGE:
            VOID memcpy( value, Calibration17MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_18_MESSAGE:
            VOID memcpy( value, Calibration18MessageValue, CALIBRATION_MSG_LENGTH );
            break;

        case CALIBRATION_19_MESSAGE:
            VOID memcpy( value, Calibration19MessageValue, CALIBRATION_MSG_LENGTH );
            break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn          Profile_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t Profile_ReadAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue, uint16_t *pLen,
                                          uint16_t offset, uint16_t maxLen,
                                          uint8_t method)
{
  bStatus_t status = SUCCESS;

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
        {
            // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
            // gattserverapp handles those reads
        case CONTROL_MESSAGE_UUID:
            *pLen = CONTROL_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, CONTROL_MSG_LENGTH );
            break;

        case SW_VERSION_MESSAGE_UUID:
            *pLen = SW_VERSION_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, SW_VERSION_MSG_LENGTH );
            break;

        case HW_VERSION_MESSAGE_UUID:
            *pLen = HW_VERSION_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, HW_VERSION_MSG_LENGTH );
            break;

        case SERIAL_NUMBER_MESSAGE_UUID:
            *pLen = SERIAL_NUMBER_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, SERIAL_NUMBER_MSG_LENGTH );
            break;

        case ENABLE_MAINTENANCE_MESSAGE_UUID:
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
            break;

        case MEASUREMENT_MESSAGE_UUID:
            *pLen = MEASUREMENT_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, MEASUREMENT_MSG_LENGTH );
            CRPSensorProfile_SetStaleBit();
            break;

        case ENABLE_MEASUREMENTS_UUID:
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
            break;

            case CRP_SYSTEM_ID_UUID:
            *pLen = SYSTEM_ID_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, SYSTEM_ID_MSG_LENGTH );
            break;

        case CAR_CORNER_UUID:
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
            break;

        case BATTERY_STATUS_UUID:
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
            break;

        case RAW_MEASUREMENT_UUID:
            *pLen = RAW_MEASUREMENT_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, RAW_MEASUREMENT_MSG_LENGTH );
            break;

	case AUTO_MEAS_PER_UUID:
		*pLen = AUTO_MEAS_PERIOD_MSG_LENGTH;
		VOID memcpy( pValue, pAttr->pValue, AUTO_MEAS_PERIOD_MSG_LENGTH );
		break;		
		
	case CRYPTO_KEY_UUID:
		*pLen = CRYPTO_KEY_MSG_LENGTH;
		VOID memcpy( pValue, pAttr->pValue, CRYPTO_KEY_MSG_LENGTH );
		break;		
		
        case ZERO_OFFSET_UUID:
            *pLen = ZERO_OFFSET_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, ZERO_OFFSET_MSG_LENGTH );
            break;

        case CALIBRATION_0_UUID:
        case CALIBRATION_1_UUID:
        case CALIBRATION_2_UUID:
        case CALIBRATION_3_UUID:
        case CALIBRATION_4_UUID:
        case CALIBRATION_5_UUID:
        case CALIBRATION_6_UUID:
        case CALIBRATION_7_UUID:
        case CALIBRATION_8_UUID:
        case CALIBRATION_9_UUID:
        case CALIBRATION_10_UUID:
        case CALIBRATION_11_UUID:
        case CALIBRATION_12_UUID:
        case CALIBRATION_13_UUID:
        case CALIBRATION_14_UUID:
        case CALIBRATION_15_UUID:
        case CALIBRATION_16_UUID:
        case CALIBRATION_17_UUID:
        case CALIBRATION_18_UUID:
        case CALIBRATION_19_UUID:
            *pLen = CALIBRATION_MSG_LENGTH;
            VOID memcpy( pValue, pAttr->pValue, CALIBRATION_MSG_LENGTH );
            break;

        default:
                *pLen = 0;
                status = ATT_ERR_ATTR_NOT_FOUND;
                break;
        }
      }
      else
      {
        // 128-bit UUID
        *pLen = 0;
        status = ATT_ERR_INVALID_HANDLE;
      }

      return ( status );
}

/*********************************************************************
 * @fn      Profile_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t Profile_WriteAttrCB(uint16_t connHandle,
                                           gattAttribute_t *pAttr,
                                           uint8_t *pValue, uint16_t len,
                                           uint16_t offset, uint8_t method)
{
  bStatus_t status = SUCCESS;
  uint8 notifyApp = 0xFF;

  uint8 *pCurValue = (uint8 *)pAttr->pValue;

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);

    switch ( uuid )
        {
        case CONTROL_MESSAGE_UUID:
                //Validate the value and make sure it's not a blob oper
                if ( offset == 0 )
                {
                    if ( len > CONTROL_MSG_LENGTH )
                    {
                        status = ATT_ERR_INVALID_VALUE_SIZE;
                    }
                }
                else
                {
                    status = ATT_ERR_ATTR_NOT_LONG;
                }

                //Write the value
                if ( status == SUCCESS )
                {
                    VOID memcpy(pAttr->pValue, pValue, CONTROL_MSG_LENGTH);
                    notifyApp = CONTROL_MESSAGE;
                }
                break;
        case SW_VERSION_MESSAGE_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > SW_VERSION_MSG_LENGTH )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, SW_VERSION_MSG_LENGTH);
                notifyApp = SW_VERSION_MESSAGE;
            }
            break;

        case HW_VERSION_MESSAGE_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > HW_VERSION_MSG_LENGTH )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, HW_VERSION_MSG_LENGTH);
                notifyApp = HW_VERSION_MESSAGE;
            }
            break;

        case SERIAL_NUMBER_MESSAGE_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > SERIAL_NUMBER_MSG_LENGTH )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, SERIAL_NUMBER_MSG_LENGTH);

                // FIXME - This equality check fails?
                //if (pAttr->pValue == (uint8_t*)&SerialNumberMessageValue )
                //{
                  notifyApp = SERIAL_NUMBER_MESSAGE;
                //}
            }
            break;

        case ENABLE_MAINTENANCE_MESSAGE_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > 1 )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }


            if ( status == SUCCESS )
            {
                *pCurValue = pValue[0];

                if( pAttr->pValue == EnableMaintenanceMessageValue )
                {
                    notifyApp = ENTER_MAINTENANCE_MESSAGE;
                }
            }
            break;


        case ENABLE_MEASUREMENTS_UUID:
              //Validate the value and make sure it's not a blob operation
              if ( offset == 0 )
              {
                if ( len > 1 )
                {
                  status = ATT_ERR_INVALID_VALUE_SIZE;
                }
              }
              else
              {
                status = ATT_ERR_ATTR_NOT_LONG;
              }

              if ( status == SUCCESS )
              {

                  *pCurValue = pValue[0];

                  if( pAttr->pValue == EnableMeasurementsMessageValue )
                  {
                     notifyApp = ENABLE_MEASUREMENTS_MESSAGE;
                  }
              }
              break;

        case CRP_SYSTEM_ID_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > SYSTEM_ID_MSG_LENGTH )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, SYSTEM_ID_MSG_LENGTH);
                notifyApp = SYSTEM_ID_MESSAGE;
            }
            break;

          case CAR_CORNER_UUID:
            if ( offset == 0 )
            {
                if ( len > 1 )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, CAR_CORNER_MSG_LENGTH);
                notifyApp = CAR_CORNER_MESSAGE;
            }
            break;

          case BATTERY_STATUS_UUID:
              if ( offset == 0 )
                {
                    if ( len > 1 )
                    {
                        status = ATT_ERR_INVALID_VALUE_SIZE;
                    }
                }
                else
                {
                    status = ATT_ERR_ATTR_NOT_LONG;
                }

                //Write the value
                if ( status == SUCCESS )
                {
                    VOID memcpy(pAttr->pValue, pValue, BATTERY_STATUS_MSG_LENGTH);
                    notifyApp = BATTERY_STATUS_MESSAGE;
                }
              break;

	case AUTO_MEAS_PER_UUID:
		//Validate the value and make sure it's not a blob oper
		if ( offset == 0 )
		{
			if ( len > AUTO_MEAS_PERIOD_MSG_LENGTH )
			{
				status = ATT_ERR_INVALID_VALUE_SIZE;
			}
		}
		else
		{
			status = ATT_ERR_ATTR_NOT_LONG;
		}

		//Write the value
		if ( status == SUCCESS )
		{
			VOID memcpy(pAttr->pValue, pValue, AUTO_MEAS_PERIOD_MSG_LENGTH);
			notifyApp = AUTO_MEAS_PERIOD_MESSAGE;
		}
		break;
		
	case CRYPTO_KEY_UUID:
		//Validate the value and make sure it's not a blob oper
              if ( offset == 0 )
                {
			if ( len > CRYPTO_KEY_MSG_LENGTH )
                    {
                        status = ATT_ERR_INVALID_VALUE_SIZE;
                    }
                }
                else
                {
                    status = ATT_ERR_ATTR_NOT_LONG;
                }

                //Write the value
                if ( status == SUCCESS )
                {
					VOID memcpy(pAttr->pValue, pValue, CRYPTO_KEY_MSG_LENGTH);
					notifyApp = CRYPTO_KEY_MESSAGE;
                }
              break;

        case ZERO_OFFSET_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > ZERO_OFFSET_MSG_LENGTH )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, ZERO_OFFSET_MSG_LENGTH);
                notifyApp = ZERO_OFFSET_MESSAGE;
            }
            break;

        case CALIBRATION_0_UUID:
        case CALIBRATION_1_UUID:
        case CALIBRATION_2_UUID:
        case CALIBRATION_3_UUID:
        case CALIBRATION_4_UUID:
        case CALIBRATION_5_UUID:
        case CALIBRATION_6_UUID:
        case CALIBRATION_7_UUID:
        case CALIBRATION_8_UUID:
        case CALIBRATION_9_UUID:
        case CALIBRATION_10_UUID:
        case CALIBRATION_11_UUID:
        case CALIBRATION_12_UUID:
        case CALIBRATION_13_UUID:
        case CALIBRATION_14_UUID:
        case CALIBRATION_15_UUID:
        case CALIBRATION_16_UUID:
        case CALIBRATION_17_UUID:
        case CALIBRATION_18_UUID:
        case CALIBRATION_19_UUID:
            //Validate the value and make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > CALIBRATION_MSG_LENGTH )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                VOID memcpy(pAttr->pValue, pValue, CALIBRATION_MSG_LENGTH);

                if(uuid == CALIBRATION_0_UUID)
                    notifyApp = CALIBRATION_0_MESSAGE;

                if(uuid == CALIBRATION_1_UUID)
                    notifyApp = CALIBRATION_1_MESSAGE;

                if(uuid == CALIBRATION_2_UUID)
                    notifyApp = CALIBRATION_2_MESSAGE;

                if(uuid == CALIBRATION_3_UUID)
                    notifyApp = CALIBRATION_3_MESSAGE;

                if(uuid == CALIBRATION_4_UUID)
                    notifyApp = CALIBRATION_4_MESSAGE;

                if(uuid == CALIBRATION_5_UUID)
                    notifyApp = CALIBRATION_5_MESSAGE;

                if(uuid == CALIBRATION_6_UUID)
                    notifyApp = CALIBRATION_6_MESSAGE;

                if(uuid == CALIBRATION_7_UUID)
                    notifyApp = CALIBRATION_7_MESSAGE;

                if(uuid == CALIBRATION_8_UUID)
                    notifyApp = CALIBRATION_8_MESSAGE;

                if(uuid == CALIBRATION_9_UUID)
                    notifyApp = CALIBRATION_9_MESSAGE;

                if(uuid == CALIBRATION_10_UUID)
                    notifyApp = CALIBRATION_10_MESSAGE;

                if(uuid == CALIBRATION_11_UUID)
                    notifyApp = CALIBRATION_11_MESSAGE;

                if(uuid == CALIBRATION_12_UUID)
                    notifyApp = CALIBRATION_12_MESSAGE;

                if(uuid == CALIBRATION_13_UUID)
                    notifyApp = CALIBRATION_13_MESSAGE;

                if(uuid == CALIBRATION_14_UUID)
                    notifyApp = CALIBRATION_14_MESSAGE;

                if(uuid == CALIBRATION_15_UUID)
                    notifyApp = CALIBRATION_15_MESSAGE;

                if(uuid == CALIBRATION_16_UUID)
                    notifyApp = CALIBRATION_16_MESSAGE;

                if(uuid == CALIBRATION_17_UUID)
                    notifyApp = CALIBRATION_17_MESSAGE;

                if(uuid == CALIBRATION_18_UUID)
                    notifyApp = CALIBRATION_18_MESSAGE;

                if(uuid == CALIBRATION_19_UUID)
                    notifyApp = CALIBRATION_19_MESSAGE;
            }
            break;


        case GATT_CLIENT_CHAR_CFG_UUID:
            status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                         offset, GATT_CLIENT_CFG_NOTIFY );
            break;

        default:
            status = ATT_ERR_ATTR_NOT_FOUND;
            break;
        }
      }
      else
      {
        // 128-bit UUID
        status = ATT_ERR_INVALID_HANDLE;
      }

      // If a charactersitic value changed then callback function to notify application of change
      if ( (notifyApp != 0xFF ) && Profile_AppCBs && Profile_AppCBs->pfnProfileChange )
      {
          Profile_AppCBs->pfnProfileChange( notifyApp );
      }

      return ( status );
    }

uint8_t CRPSensorProfile_MeasurementRead()
{
    return bMeasurementRead;
}

void CRPSensorProfile_ClearMeasurement()
{
    MeasurementMessageValue[0] = 0;
    MeasurementMessageValue[1] = 0;
    MeasurementMessageValue[2] = 0;
    MeasurementMessageValue[3] = 0;
}

void CRPSensorProfile_SetFreshBit()
{
    MeasurementMessageValue[3] |= 0x80;
    bMeasurementRead = 0;
}

void CRPSensorProfile_SetStaleBit()
{
    MeasurementMessageValue[3] &= 0x7F;
}

void CRPSensorProfile_SetBusyBit()
{
    MeasurementMessageValue[3] |= 0x02;
}

void CRPSensorProfile_SetIdleBit()
{
    MeasurementMessageValue[3] &= 0xFD;
}

void CRPSensorProfile_SetTimeoutBit()
{
    MeasurementMessageValue[3] |= 0x40;
}

void CRPSensorProfile_ClearTimeoutBit()
{
    MeasurementMessageValue[3] &= 0xAF; // clear both busy and timeout bits
}

/*********************************************************************
*********************************************************************/
