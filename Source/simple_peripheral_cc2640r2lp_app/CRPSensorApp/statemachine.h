#ifndef _STATEMACHINE_H_
#define _STATEMACHINE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

typedef enum
{
    STATE_INIT = 0,
    STATE_DISABLE_LASER = 1,
	STATE_IDLE = 2,
	STATE_AUTO_MEAS = 3,
	STATE_SHUTDOWN = 4,
	STATE_UNKNOWN = 5,
	STATE_FLASH_FIVE_TIMES_AND_SHUTDOWN = 6
} STATE_TYPE;

void STATE_Init(void);
void STATE_Task(void);
uint8_t STATE_GetState(void);

void STATE_EnableMaintenance(void);
void STATE_DisableMaintenance(void);
uint8_t STATE_IsMaintenanceEnabled(void);
void STATE_RequestState(STATE_TYPE eNewState);
void STATE_ResetModeTimer(void);
uint8_t STATE_ShouldShutdown(void);

#ifdef __cplusplus
}
#endif

#endif
