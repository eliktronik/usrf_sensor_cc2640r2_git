/*
 * broadcast.h
 *
 *  Created on: Jun 26, 2020
 *      Author: elik
 */

#ifndef CRPSENSORAPP_BROADCAST_H_
#define CRPSENSORAPP_BROADCAST_H_

#define BROADCAST_BYTEPOS_MSGOFFSET 9

#define BROADCAST_DYNAMIC_LEN		16

#define BROADCAST_BYTEPOS_SWVER      0
#define BROADCAST_BYTEPOS_HWVER      1
#define BROADCAST_BYTEPOS_MSGSEQ     2
#define BROADCAST_BYTEPOS_MEAS_WHOLE 3
#define BROADCAST_BYTEPOS_MEAS_FRAC0 4
#define BROADCAST_BYTEPOS_MEAS_FRAC1 5
#define BROADCAST_BYTEPOS_ZERO_SEQ   6
#define BROADCAST_BYTEPOS_ZERO_WHOLE 7
#define BROADCAST_BYTEPOS_ZERO_FRAC0 8
#define BROADCAST_BYTEPOS_ZERO_FRAC1 9
#define BROADCAST_BYTEPOS_MEAS_PER   10
#define BROADCAST_BYTEPOS_BATTERY 	 11
//#define BROADCAST_BYTEPOS_SWVER      12
//#define BROADCAST_BYTEPOS_SWVER      13
#define BROADCAST_BYTEPOS_FIXED0     14
#define BROADCAST_BYTEPOS_FIXED1     15

/*
 * #define BROADCAST_BYTEPOS_SWVER      15
#define BROADCAST_BYTEPOS_HWVER      14
#define BROADCAST_BYTEPOS_MSGSEQ     13
#define BROADCAST_BYTEPOS_MEAS_WHOLE 12
#define BROADCAST_BYTEPOS_MEAS_FRAC0 11
#define BROADCAST_BYTEPOS_MEAS_FRAC1 10
#define BROADCAST_BYTEPOS_ZERO_SEQ   9
#define BROADCAST_BYTEPOS_ZERO_WHOLE 8
#define BROADCAST_BYTEPOS_ZERO_FRAC0 7
#define BROADCAST_BYTEPOS_ZERO_FRAC1 6
#define BROADCAST_BYTEPOS_MEAS_PER   5
#define BROADCAST_BYTEPOS_BATTERY    4
//#define BROADCAST_BYTEPOS_SWVER      12
//#define BROADCAST_BYTEPOS_SWVER      13
#define BROADCAST_BYTEPOS_FIXED0     1
#define BROADCAST_BYTEPOS_FIXED1     0
 *
 */

// fixed data to help validate decryption
#define BROADCAST_FIXED_FIELD1_DATA  0x79
#define BROADCAST_FIXED_FIELD0_DATA  0xC0

// Helper #def fcn macros
#define ASCII_TO_BIN(x)			(x - 0x30)

void BroadcastUpdateMeasurement (volatile uint8_t [], uint8_t *, uint8_t );
void BroadcastUpdateZero (volatile uint8_t [], uint8_t *);
void BroadcastUpdateBatteryLvl (volatile uint8_t [], uint8_t);
void BroadcastUpdateAutoMeasPeriod (volatile uint8_t [], uint8_t);
void BroadcastUpdateDynamicPayload (uint8_t * , volatile uint8_t * );
uint8_t CheckCryptoKeyBlank ( uint8_t [] );
void memcpyReverse(uint8_t *, uint8_t *, uint32_t);



#endif /* CRPSENSORAPP_BROADCAST_H_ */
