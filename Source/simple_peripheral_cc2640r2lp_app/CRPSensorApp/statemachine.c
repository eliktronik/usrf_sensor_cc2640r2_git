#include "statemachine.h"
#include "../Application/simple_peripheral.h"
#include <xdc/std.h>

#define MODE_TIMER_INCREMENT	1 	// 1 second ticks
#define TWO_HOURS_IN_SECONDS	7200
#define TEN_MINUTES_IN_SECONDS  600

// Local variables
STATE_TYPE eCurrentState = STATE_UNKNOWN;
STATE_TYPE eRequestedState = STATE_INIT;


// Internal variables
uint8_t ui8MaintenanceMode = 0;
uint16_t uiModeTimer = 0;
uint8_t ui8ShouldShutdown = 0;
uint8_t ui8CryptoWriteMode = FALSE;

// Internal functions
void STATEi_StateExit(STATE_TYPE eState);
void STATEi_StateEntry(STATE_TYPE eState);

// Internal Mode Processing functions
void STATEi_InitState(void);
void STATEi_DisableLaser(void);
void STATEi_IdleState(void);
void STATEi_AutoMeas(void);
void STATEi_Shutdown(void);

void STATE_Init(void)
{
	ui8MaintenanceMode = 0;
	uiModeTimer = 0;
	ui8ShouldShutdown = 0;

	eCurrentState = STATE_UNKNOWN;
	eRequestedState = STATE_INIT;
}

void STATE_ResetModeTimer(void)
{
	uiModeTimer = 0;
}

uint8_t STATE_ShouldShutdown(void)
{
	return ui8ShouldShutdown;
}

void STATE_Task(void)
{
	if(eCurrentState != eRequestedState)
	{
		STATEi_StateExit(eCurrentState);
		STATEi_StateEntry(eRequestedState);

		eCurrentState = eRequestedState;
	}

	uiModeTimer += MODE_TIMER_INCREMENT;

	switch(eCurrentState)
	{
	case STATE_INIT:
		STATEi_InitState();
		break;
	case STATE_DISABLE_LASER:
		STATEi_DisableLaser();
		break;
	case STATE_IDLE:
		STATEi_IdleState();
		break;
	case STATE_AUTO_MEAS:
		STATEi_AutoMeas();
		break;
	case STATE_SHUTDOWN:
		STATEi_Shutdown();
		break;
	case STATE_FLASH_FIVE_TIMES_AND_SHUTDOWN:
	    break;

	// We shouldn't get here, but if we do, reset to a safe state.
	default:
		STATE_Init();
		break;
	}
}


uint8_t STATE_GetState(void)
{
	return (uint8_t) eCurrentState;
}

// --- Set and check maintenance (3 fcns)
void STATE_EnableMaintenance(void)
{
	ui8MaintenanceMode = 1;
}

void STATE_DisableMaintenance(void)
{
	ui8MaintenanceMode = 0;
}

uint8_t STATE_IsMaintenanceEnabled(void)
{
	return ui8MaintenanceMode;
}

void STATE_RequestState(STATE_TYPE eNewState)
{
	eRequestedState = eNewState;
}

void STATEi_StateExit(STATE_TYPE eState)
{
	switch(eState)
		{
		case STATE_INIT:
			break;
		case STATE_DISABLE_LASER:
			break;
		case STATE_IDLE:
			break;
		case STATE_AUTO_MEAS:
			ResetAutoMeasurePeriod();	// let bleSimplePeripheral know we've timed out
			break;
		case STATE_SHUTDOWN:
			break;
		case STATE_FLASH_FIVE_TIMES_AND_SHUTDOWN:
            break;

		// We shouldn't get here, but if we do, reset to a safe state.
		default:
			STATE_Init();
			break;
		}
}


void STATEi_StateEntry(STATE_TYPE eState)
{
	switch(eState)
		{
		case STATE_INIT:
			break;
		case STATE_DISABLE_LASER:
			break;
		case STATE_IDLE:
			break;
		case STATE_AUTO_MEAS:
			break;
		case STATE_SHUTDOWN:
			break;
		case STATE_FLASH_FIVE_TIMES_AND_SHUTDOWN:
            break;

		// We shouldn't get here, but if we do, reset to a safe state.
		default:
			STATE_Init();
			break;
		}
}


void STATEi_InitState(void)
{
	if(uiModeTimer >= 1)
	{
		eRequestedState = STATE_DISABLE_LASER;
		uiModeTimer = 0;
	}
}


void STATEi_DisableLaser(void)
{
	if(uiModeTimer >= 3)
	{
		eRequestedState = STATE_IDLE;
		uiModeTimer = 0;
	}
}

void STATEi_IdleState(void)
{
	// If we're idle for 2 or more hours, shutdown.
	if(uiModeTimer >= TWO_HOURS_IN_SECONDS)
	{
		ui8ShouldShutdown = 1;
		eRequestedState = STATE_SHUTDOWN;
	}
}

void STATEi_AutoMeas(void)
{
	// if we're idle for ten minutes, stop measuring
	if (uiModeTimer >= TEN_MINUTES_IN_SECONDS) {
		eRequestedState = STATE_IDLE;
	}
}

void STATEi_Shutdown(void)
{
	// Stay put
}
