/*
 * broadcast.c
 *
 *  Created on: Jun 26, 2020
 *      Author: Elik Dvorkin
 *
 *	(C) Copyright 2020 Creative Racing Products, LLC
 *
 *	This file implements some functions to help with broadcast message generation
 *
 */

#include <stdint.h>
#include <xdc/std.h>
#include <stdbool.h>

#include "broadcast.h"

static uint8_t MeasurementSequence = 0;
static uint8_t ZeroSequence = 0;


// Update Measurement Value
inline void BroadcastUpdateMeasurement (volatile uint8_t msg[], uint8_t * Measurement, uint8_t ClearSequence) {

	uint8_t LocalMeasurement[4] = {0,0,0,0};

	memcpy( LocalMeasurement, Measurement, 4);

	// Update measurement - integer
	msg[BROADCAST_BYTEPOS_MEAS_WHOLE] =
			LocalMeasurement[2];

	msg[BROADCAST_BYTEPOS_MEAS_FRAC0] =
			LocalMeasurement[1];

	msg[BROADCAST_BYTEPOS_MEAS_FRAC1] =
			LocalMeasurement[0];

	// update sequence & send
	if (ClearSequence == true)
		MeasurementSequence = 0;
	else
		MeasurementSequence++;

	msg[BROADCAST_BYTEPOS_MSGSEQ] =  MeasurementSequence;

}

// Update Zero Value
inline void BroadcastUpdateZero (volatile uint8_t * msg, uint8_t * Zero) {

	// Update measurement - integer
	msg[BROADCAST_BYTEPOS_ZERO_WHOLE]
			= Zero[2];

	msg[BROADCAST_BYTEPOS_ZERO_FRAC0]
			= Zero[1];

	msg[BROADCAST_BYTEPOS_ZERO_FRAC1]
			= Zero[0];

	// update sequence & send
	msg[BROADCAST_BYTEPOS_ZERO_SEQ]
		= ++ZeroSequence;
}

// Memcpy with byte reversal because SDK v1.50.xx.xx AES encryption function reverses byte order (why?)
void memcpyReverse(uint8_t * destination, uint8_t * source, uint32_t numbytes) {

    uint32_t i = 0;
    for (i = 0; i<numbytes; i++) {
        destination[numbytes - 1 - i] = source[i];

    }

}


// Battery
inline void BroadcastUpdateBatteryLvl (volatile uint8_t msg[], uint8_t BatteryLevel) {

	// update sequence & send
	msg[BROADCAST_BYTEPOS_BATTERY] = BatteryLevel;
}

// Copy Dynamic Data into AdvertData string
inline void BroadcastUpdateDynamicPayload (uint8_t * msg, volatile uint8_t * DynamicMsg) {

	memcpy( msg + BROADCAST_BYTEPOS_MSGOFFSET, (uint8_t *) DynamicMsg, BROADCAST_DYNAMIC_LEN);
}

// Check for blank key in order to decide whether to send ciphertext or plaintext
inline uint8_t CheckCryptoKeyBlank ( uint8_t key[]) {
	uint8_t i = 0;
	for (i=0;i<BROADCAST_DYNAMIC_LEN; i++) {

		if (key[i] != 0)
			return false;
	}
	return true;
}

// Battery
inline void BroadcastUpdateAutoMeasPeriod (volatile uint8_t * msg, uint8_t Period) {

	// update sequence & send
	msg[BROADCAST_BYTEPOS_MEAS_PER] = Period;
}
