/**************************************************************************************************
  Filename:       simpleBLEperipheral.h
  Revised:        $Date: 2014-04-16 15:24:18 -0700 (Wed, 16 Apr 2014) $
  Revision:       $Revision: 38209 $

  Description:    This file contains the rangefinder calculcations definitions and prototypes.

  Copyright 2013 - Creative Racing Products, LLC

**************************************************************************************************/

#ifndef RANGEFINDERCALCS_H
#define RANGEFINDERCALCS_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include "stdint.h"


/*********************************************************************
*  EXTERNAL VARIABLES
*/

typedef struct ProcessedMeasurement {
	uint32_t val;
	uint16_t modeCount;
} processedMeasurement_t;

/*********************************************************************
 * MACROS
 */
#define POW2_TO_8 		256
#define POW2_TO_16 		65536
#define POW2_TO_24 		16777216

// length of calibration array
#define CALIBRATION_ARRAY_LENGTH			20								// length of calibration array
#define CALIBRATION_ARRAY_INUSE				19

 
/*********************************************************************
 * FUNCTIONS
 */
processedMeasurement_t MeasurementArrayModeSearch(uint32_t * , uint16_t );

float SpeedOfSound(int16_t);

uint32_t DistanceCalculation (uint32_t, uint16_t, int16_t );

uint32_t DistanceCorrection (uint32_t, int32_t [CALIBRATION_ARRAY_LENGTH][2]);

/*********************************************************************
 * CONSTANTS
 */


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SIMPLEBLEPERIPHERAL_H */

