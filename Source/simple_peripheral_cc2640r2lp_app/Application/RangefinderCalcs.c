/**************************************************************************************************
 Filename:       RangefinderCalcs.c

 Description:    This file contains computations specific to the rangefinder functionality.

 Copyright 2016 - Creative Racing Products, LLC
*/

#include "RangefinderCalcs.h"
#include "simple_peripheral.h"

#define CLK_FREQ_MHZ						48
#define CLK_FREQ							(CLK_FREQ_MHZ * 1e6)
#define TRANSDUCER_FREQ_KHZ					300
#define TRANSDUCER_PER_CNTS					(TRANSDUCER_FREQ_KHZ * 1000 / TRANSDUCER_FREQ_KHZ)
#define MEAS_GROUP_TOLERANCE				(TRANSDUCER_PER_CNTS / 2)		// clock ticks separating one 300kHz period from the next (half-period time)

#define DEFAULT_OFFSET						(0.5f)			// assume the default transducer height is 0.5"

#define DISTANCE_MASK						0xFFFFFFC0		// Distance masking
#define DISTANCE_MASK_CHECK					0x00000020		// bit to check for rounding

#define SPEED_OF_SOUND_ZERO_C 				13044f

/*********************************************************************
 * @fn      MeasurementArrayModeSearch
 *
 * @brief   Finds a mode group of the a measurement array, and averages the group.
 *
 * @param   pointer to the measurement buffer, number of measurements.
 *
 * @return  None.
 *********************************************************************/ 
 
processedMeasurement_t MeasurementArrayModeSearch(uint32_t *TimerBuf, uint16_t NumMeasurements)
{

	uint32_t uiAverage = 0;
	uint32_t temp = 0;
	uint8_t i = 0;
	uint8_t j = 0;
	uint8_t curGroupID = 0;			// track group IDs in mode
	uint8_t modeStartNdx = 0;
	uint8_t modeEndNdx = NumMeasurements - 1;
	uint8_t modeCount = 0;
	uint8_t thisGroupStartNdx = 0;
	uint8_t thisGroupCount = 0;
	uint8_t  measGroups[TIMERBUFSIZE] = { 0 };      // tracks which measurements are in which group
	processedMeasurement_t measurement;

	// Bubble sort the array first (this could be more efficient, but who cares), ascending
	for (i = 1; i < NumMeasurements; i++)
	{
		for (j = 1; j < NumMeasurements; j++)
		{
			if (TimerBuf[j] > TimerBuf[j+1]) {		// if larger than next val, push it up
				temp = TimerBuf[j+1];
				TimerBuf[j+1] = TimerBuf[j];
				TimerBuf[j] = temp;
			}
		}
	}

	// Group the array values into ranges in order to find the mode (within a tolerance band).
	// For each TimerBuf value, plug a group ID value into measGroups representing its grouping.
	// Ignore first (index 1) and last (min and max)
	measGroups[2] = 0;
	for (i = 3; i < NumMeasurements; i++)			// ignore 1 and look back, so start at 3
	{
		if (TimerBuf[i] > (uint32_t)(TimerBuf[i-1] + MEAS_GROUP_TOLERANCE) )
		{
			measGroups[i] = ++curGroupID;			// increment and set new ID
		}
		else {
			measGroups[i] = curGroupID;				// same ID
		}

	}

	// identify the mode. count groups in the array and find the most prevalent one.
	thisGroupCount = 1;
	thisGroupStartNdx = 2;		// first valid entry
	modeStartNdx = 2;			// default mode start
	for (i = 3; i < NumMeasurements; i++)
	{
		if (measGroups[i] == measGroups[i-1])	// same group
		{
			thisGroupCount++;							// TODO: Do something to reject the zero-value group.

		} else {								// starting new group

			thisGroupCount = 1;					// restart counter
			thisGroupStartNdx = i;				// re-set start index

		}

		if (thisGroupCount > modeCount)			// found new mode
		{
			modeCount = thisGroupCount;			// save mode count
			modeStartNdx = thisGroupStartNdx;	// save start index
			modeEndNdx = i;						// assume this index is the end for now...
		}

	}

	// Now do our average on the mode group.
	for (i = modeStartNdx; i <= modeEndNdx; i++)
	{
		uiAverage += TimerBuf[i];
	}

	// Compute the average.
	uiAverage /=  modeCount;

	measurement.val = uiAverage;
	measurement.modeCount = modeCount;
	
	return measurement;
	
}

// disable optimizations here
#pragma FUNCTION_OPTIONS(DistanceCalculation,"--opt_level=0")

uint32_t DistanceCalculation (uint32_t timerVal, uint16_t holdoffClocks, int16_t temperature )
{
	float Distance = 0;
	uint32_t uiDistance = 0;

	Distance = (float)( (uint32_t)timerVal + (uint32_t)holdoffClocks);	// add back holdoff time. In units of clock ticks.
	Distance *= SpeedOfSound(temperature) / 2.0f / (float)CLK_FREQ;		// multiply total time by speed, divide by two and convert clock ticks to seconds.
	Distance = Distance + DEFAULT_OFFSET;								// add in offset
	Distance *= (float)POW2_TO_16;										// adjust for 16.16 format POW2_TO_16

	uiDistance = (uint32_t)(Distance );

	// Mask useless bits but round first
	if (uiDistance & (DISTANCE_MASK_CHECK))		// check for need to round
		uiDistance = (uiDistance & DISTANCE_MASK)  + (DISTANCE_MASK_CHECK << 1);		// increment LSB
	else
		uiDistance = (uiDistance & DISTANCE_MASK);

	return uiDistance;						// return uint

}


/*********************************************************************
 * @fn      DistanceCorrection
 *
 * @brief   Adds in calibration offset value to the distance measured. Note: Calibration array needs to be completely populated.
 *
 * @param   distance, calibration array(index)(0=dist, 1=corr), cal array length
 *
 * @return  Distance in 16.16b uint
 */
uint32_t DistanceCorrection (uint32_t uiDistance, int32_t CalibrationArray[CALIBRATION_ARRAY_LENGTH][2])
{

	uint8_t i = 0;
	int32_t correction = 0;

	// map into the array - using a subset
	for ( i=0; i < CALIBRATION_ARRAY_INUSE; i++ ) {
		if (uiDistance < CalibrationArray[i][0] )
			break;
	}

	if (i > CALIBRATION_ARRAY_INUSE-1 ) {				// check for falling off the end of the array

		correction = CalibrationArray[CALIBRATION_ARRAY_INUSE-1][1];

	} else if ( i == 0 ) {		// or below the minimum distance

		correction = CalibrationArray[0][1];

	} else {												// middle of array, run linear interp

		float fCorrection = 0;
		fCorrection = 	(float)( CalibrationArray[i][1] - CalibrationArray[i-1][1] );		// find slope num (RSH to avoid overflow)
		fCorrection /= 	(float)( CalibrationArray[i][0] - CalibrationArray[i-1][0] );		// div by slope denom
		fCorrection *=	(float)( (int32_t)uiDistance - CalibrationArray[i-1][0]);			// multiple by local x

		fCorrection += 	(float)CalibrationArray[i-1][1];					// add in y offset
		correction = (int32_t)fCorrection;							// cast back to int
	}

	uiDistance = (uint32_t) ( ((int32_t)uiDistance ) - correction );							// doesn't need range check. Will never wrap.
									//) ) & 0xFFFFFF00 ) | ( ((uint32_t)i)) ;		// debug output: i to characteristic

	return uiDistance;


}

/*********************************************************************
 * @fn      SpeedOfSound
 *
 * @brief   Returns speed of sound for a particular temp.
 *
 * @param   A temperature (celsius) in 12.4 format.
 *
 * @return  Float speed of sound in inches per clock tick.
 */

float SpeedOfSound(int16_t temp_by_16)
{
	
	// this array is indexed by temperature, 6.3b unsigned (half precision of input param), offset so that -5C = index 0
	// range is -5 C to +44.875 C
	static const float SpeedOfSound_Minus5C_Eighths[400] = {
	12923.997f	,
	12927.011f	,
	12930.024f	,
	12933.036f	,
	12936.048f	,
	12939.059f	,
	12942.069f	,
	12945.078f	,
	12948.087f	,
	12951.095f	,
	12954.102f	,
	12957.109f	,
	12960.115f	,
	12963.120f	,
	12966.125f	,
	12969.129f	,
	12972.132f	,
	12975.134f	,
	12978.136f	,
	12981.137f	,
	12984.137f	,
	12987.137f	,
	12990.136f	,
	12993.134f	,
	12996.132f	,
	12999.129f	,
	13002.125f	,
	13005.121f	,
	13008.115f	,
	13011.110f	,
	13014.103f	,
	13017.096f	,
	13020.088f	,
	13023.079f	,
	13026.070f	,
	13029.060f	,
	13032.049f	,
	13035.038f	,
	13038.026f	,
	13041.013f	,
	13044.000f	,
	13046.986f	,
	13049.971f	,
	13052.956f	,
	13055.940f	,
	13058.923f	,
	13061.905f	,
	13064.887f	,
	13067.868f	,
	13070.849f	,
	13073.829f	,
	13076.808f	,
	13079.786f	,
	13082.764f	,
	13085.741f	,
	13088.717f	,
	13091.693f	,
	13094.668f	,
	13097.642f	,
	13100.616f	,
	13103.589f	,
	13106.562f	,
	13109.533f	,
	13112.504f	,
	13115.475f	,
	13118.444f	,
	13121.413f	,
	13124.381f	,
	13127.349f	,
	13130.316f	,
	13133.282f	,
	13136.248f	,
	13139.213f	,
	13142.177f	,
	13145.141f	,
	13148.104f	,
	13151.066f	,
	13154.028f	,
	13156.989f	,
	13159.949f	,
	13162.909f	,
	13165.868f	,
	13168.826f	,
	13171.783f	,
	13174.740f	,
	13177.697f	,
	13180.652f	,
	13183.607f	,
	13186.562f	,
	13189.515f	,
	13192.468f	,
	13195.421f	,
	13198.372f	,
	13201.323f	,
	13204.274f	,
	13207.223f	,
	13210.172f	,
	13213.121f	,
	13216.068f	,
	13219.015f	,
	13221.962f	,
	13224.908f	,
	13227.853f	,
	13230.797f	,
	13233.741f	,
	13236.684f	,
	13239.626f	,
	13242.568f	,
	13245.509f	,
	13248.450f	,
	13251.390f	,
	13254.329f	,
	13257.268f	,
	13260.205f	,
	13263.143f	,
	13266.079f	,
	13269.015f	,
	13271.951f	,
	13274.885f	,
	13277.819f	,
	13280.753f	,
	13283.685f	,
	13286.617f	,
	13289.549f	,
	13292.479f	,
	13295.410f	,
	13298.339f	,
	13301.268f	,
	13304.196f	,
	13307.124f	,
	13310.051f	,
	13312.977f	,
	13315.902f	,
	13318.827f	,
	13321.752f	,
	13324.675f	,
	13327.598f	,
	13330.521f	,
	13333.443f	,
	13336.364f	,
	13339.284f	,
	13342.204f	,
	13345.123f	,
	13348.042f	,
	13350.960f	,
	13353.877f	,
	13356.794f	,
	13359.710f	,
	13362.625f	,
	13365.540f	,
	13368.454f	,
	13371.367f	,
	13374.280f	,
	13377.192f	,
	13380.104f	,
	13383.015f	,
	13385.925f	,
	13388.835f	,
	13391.744f	,
	13394.652f	,
	13397.560f	,
	13400.467f	,
	13403.374f	,
	13406.280f	,
	13409.185f	,
	13412.089f	,
	13414.993f	,
	13417.897f	,
	13420.799f	,
	13423.702f	,
	13426.603f	,
	13429.504f	,
	13432.404f	,
	13435.304f	,
	13438.203f	,
	13441.101f	,
	13443.999f	,
	13446.896f	,
	13449.792f	,
	13452.688f	,
	13455.583f	,
	13458.478f	,
	13461.372f	,
	13464.265f	,
	13467.158f	,
	13470.050f	,
	13472.942f	,
	13475.833f	,
	13478.723f	,
	13481.613f	,
	13484.502f	,
	13487.390f	,
	13490.278f	,
	13493.165f	,
	13496.051f	,
	13498.937f	,
	13501.823f	,
	13504.707f	,
	13507.592f	,
	13510.475f	,
	13513.358f	,
	13516.240f	,
	13519.122f	,
	13522.003f	,
	13524.883f	,
	13527.763f	,
	13530.642f	,
	13533.521f	,
	13536.399f	,
	13539.276f	,
	13542.153f	,
	13545.029f	,
	13547.904f	,
	13550.779f	,
	13553.653f	,
	13556.527f	,
	13559.400f	,
	13562.273f	,
	13565.144f	,
	13568.016f	,
	13570.886f	,
	13573.756f	,
	13576.626f	,
	13579.494f	,
	13582.363f	,
	13585.230f	,
	13588.097f	,
	13590.964f	,
	13593.829f	,
	13596.695f	,
	13599.559f	,
	13602.423f	,
	13605.286f	,
	13608.149f	,
	13611.011f	,
	13613.873f	,
	13616.734f	,
	13619.594f	,
	13622.454f	,
	13625.313f	,
	13628.172f	,
	13631.030f	,
	13633.887f	,
	13636.744f	,
	13639.600f	,
	13642.456f	,
	13645.311f	,
	13648.165f	,
	13651.019f	,
	13653.872f	,
	13656.724f	,
	13659.576f	,
	13662.428f	,
	13665.279f	,
	13668.129f	,
	13670.978f	,
	13673.827f	,
	13676.676f	,
	13679.524f	,
	13682.371f	,
	13685.218f	,
	13688.064f	,
	13690.909f	,
	13693.754f	,
	13696.598f	,
	13699.442f	,
	13702.285f	,
	13705.127f	,
	13707.969f	,
	13710.811f	,
	13713.651f	,
	13716.492f	,
	13719.331f	,
	13722.170f	,
	13725.008f	,
	13727.846f	,
	13730.683f	,
	13733.520f	,
	13736.356f	,
	13739.192f	,
	13742.026f	,
	13744.861f	,
	13747.694f	,
	13750.528f	,
	13753.360f	,
	13756.192f	,
	13759.023f	,
	13761.854f	,
	13764.684f	,
	13767.514f	,
	13770.343f	,
	13773.172f	,
	13775.999f	,
	13778.827f	,
	13781.653f	,
	13784.480f	,
	13787.305f	,
	13790.130f	,
	13792.954f	,
	13795.778f	,
	13798.602f	,
	13801.424f	,
	13804.246f	,
	13807.068f	,
	13809.889f	,
	13812.709f	,
	13815.529f	,
	13818.348f	,
	13821.167f	,
	13823.985f	,
	13826.802f	,
	13829.619f	,
	13832.436f	,
	13835.251f	,
	13838.066f	,
	13840.881f	,
	13843.695f	,
	13846.509f	,
	13849.322f	,
	13852.134f	,
	13854.946f	,
	13857.757f	,
	13860.567f	,
	13863.377f	,
	13866.187f	,
	13868.996f	,
	13871.804f	,
	13874.612f	,
	13877.419f	,
	13880.226f	,
	13883.032f	,
	13885.837f	,
	13888.642f	,
	13891.447f	,
	13894.250f	,
	13897.054f	,
	13899.856f	,
	13902.659f	,
	13905.460f	,
	13908.261f	,
	13911.061f	,
	13913.861f	,
	13916.661f	,
	13919.459f	,
	13922.257f	,
	13925.055f	,
	13927.852f	,
	13930.649f	,
	13933.445f	,
	13936.240f	,
	13939.035f	,
	13941.829f	,
	13944.623f	,
	13947.416f	,
	13950.208f	,
	13953.000f	,
	13955.792f	,
	13958.583f	,
	13961.373f	,
	13964.163f	,
	13966.952f	,
	13969.740f	,
	13972.529f	,
	13975.316f	,
	13978.103f	,
	13980.890f	,
	13983.675f	,
	13986.461f	,
	13989.245f	,
	13992.030f	,
	13994.813f	,
	13997.596f	,
	14000.379f	,
	14003.161f	,
	14005.942f	,
	14008.723f	,
	14011.504f	,
	14014.283f	,
	14017.063f	,
	14019.841f	,
	14022.619f	,
	14025.397f	,
	14028.174f	,
	14030.951f	,
	14033.726f	,
	14036.502f	,
	14039.277f	,
	14042.051f	,
	14044.825f	,
	14047.598f	,
	14050.371f	,
	14053.143f	,
	14055.914f	,
	14058.685f	,
	14061.456f	,
	14064.226f	,
	14066.995f	,
	14069.764f	,
	14072.532f	,
	14075.300f
	} ;


	uint16_t index = 0;

	if (temp_by_16 < (-5 * 16) ) // check for too low a value
		index = 0;
	else if (temp_by_16 > ((44 * 16) + 14) ) // check for 44.875 = 44 7/8 = 44 * 16 + (7/8 * 16)
		index = 399;
	else
		index = (uint16_t)( (temp_by_16 / 2) - (-5 * 8) ); 	// decimate to eighths, add 5C offset
			
	if (index > 399)
		index  = 399;

	return  SpeedOfSound_Minus5C_Eighths[index];

}
